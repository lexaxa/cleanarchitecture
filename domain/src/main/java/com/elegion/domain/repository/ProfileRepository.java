package com.elegion.domain.repository;

import com.elegion.domain.model.user.User;

import io.reactivex.Single;

/**
 * Created by tanchuev on 24.04.2018.
 */

public interface ProfileRepository {
    String SERVER = "SERVER";
    String DB = "DB";

    Single<User> getUser(String username);

    void insertUser(User user);

}
