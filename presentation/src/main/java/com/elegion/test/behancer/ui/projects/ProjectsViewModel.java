package com.elegion.test.behancer.ui.projects;

import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.support.v4.widget.SwipeRefreshLayout;

import com.elegion.domain.model.project.Project;
import com.elegion.domain.service.ProjectService;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ProjectsViewModel {

    private ProjectsAdapter.OnItemClickListener mOnItemClickListener;
    private ObservableBoolean mIsErrorVisible = new ObservableBoolean(false);
    private ObservableBoolean mIsLoading = new ObservableBoolean(false);
    private ObservableArrayList<Project> mProjects = new ObservableArrayList<>();
    private SwipeRefreshLayout.OnRefreshListener mOnRefreshListener = this::loadProjects;

    @Inject
    ProjectService mService;

    @Inject
    public ProjectsViewModel(){}

    public ProjectsViewModel(ProjectsAdapter.OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public void loadProjects() {
        mCompositeDisposable.add(
                mService.getProjects()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(disposable -> mIsLoading.set(true))
                        .doFinally(() -> mIsLoading.set(false))
                        .subscribe(
                                response -> {
                                    mIsErrorVisible.set(false);
                                    mProjects.addAll(response);
                                },
                                throwable -> mIsErrorVisible.set(true))
        );
    }

    public ProjectsAdapter.OnItemClickListener getOnItemClickListener() {
        return mOnItemClickListener;
    }

    public ObservableBoolean getIsErrorVisible() {
        return mIsErrorVisible;
    }

    public ObservableBoolean getIsLoading() {
        return mIsLoading;
    }

    public ObservableArrayList<Project> getProjects() {
        return mProjects;
    }

    public SwipeRefreshLayout.OnRefreshListener getOnRefreshListener() {
        return mOnRefreshListener;
    }

    protected final CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    public void dispatchDetach() {
        if(mCompositeDisposable != null) {
            mCompositeDisposable.dispose();
        }
    }
}
