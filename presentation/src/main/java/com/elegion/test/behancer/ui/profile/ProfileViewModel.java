package com.elegion.test.behancer.ui.profile;

import com.elegion.domain.service.ProfileService;
import com.elegion.domain.service.ProjectService;
import com.elegion.test.behancer.common.BasePresenter;
import com.elegion.test.behancer.ui.projects.ProjectsView;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Vladislav Falzan.
 */

public class ProfileViewModel {

    private ProfileView mView;
    @Inject
    ProfileService mService;

    @Inject
    public ProfileViewModel() {
    }

    public void setView(ProfileView view) {
        mView = view;
    }

    public void getUser(String username) {
        mCompositeDisposable.add(
                mService.getUser(username)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(disposable -> mView.showRefresh())
                        .doFinally(mView::hideRefresh)
                        .subscribe(
                                response -> mView.showUser(response),
                                throwable -> mView.showError())
        );
    }

    protected final CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    public void disposeAll() {
        mCompositeDisposable.dispose();
    }
}
