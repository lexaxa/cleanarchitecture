package com.elegion.test.behancer.ui.profile;

import android.support.annotation.NonNull;

import com.elegion.domain.model.project.Project;
import com.elegion.domain.model.user.User;
import com.elegion.test.behancer.common.BaseView;

import java.util.List;

/**
 * Created by Vladislav Falzan.
 */

public interface ProfileView extends BaseView {

    void showUser(@NonNull User user);

}
